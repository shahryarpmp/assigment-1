import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PlayingPanel extends JPanel {

    private ShowPanel showPanel = new ShowPanel() ;
    private ShapeTools shapeTools = new ShapeTools() ;
    private ShapePanel shapePanel = new ShapePanel() ;
    private AnimatTools animatTools = new AnimatTools() ;
    private AnimatePanel animatePanel = new AnimatePanel() ;

    public PlayingPanel(ShowPanel showPanel, AnimatePanel animatePanel, AnimatTools animatTools, ShapePanel shapePanel, ShapeTools shapeTools) {
        this.shapePanel = shapePanel ;
        this.shapeTools = shapeTools ;
        this.animatePanel = animatePanel ;
        this.animatTools = animatTools ;
        this.showPanel = showPanel ;
        this.setBackground(Color.BLUE);
        init();

    }

    public void init(){

        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString() + "/src/pics/";

        JButton play = new JButton();
        //
        BufferedImage animPlayer = null;
        try {
            animPlayer = ImageIO.read(new File(path + "play.png" ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Graphics2D g2 = animPlayer.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        g2.drawImage(animPlayer, 0, 0, null);
        g2.dispose();
        play.setIcon(new ImageIcon(animPlayer));
        this.add(play);

        JButton pause = new JButton();
        //
        BufferedImage animPauser = null;
        try {
            animPauser = ImageIO.read(new File(path + "pause.png" ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        g2.drawImage(animPauser, 0, 0, null);
        g2.dispose();
        pause.setIcon(new ImageIcon(animPauser));


        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PlayingPanel.this.remove(play);
                PlayingPanel.this.add(pause);
                PlayingPanel.this.validate();

                shapePanel.clear() ;
                shapeTools.clear();
                animatePanel.clear();
                animatTools.clear();

                shapePanel.validate();
                shapeTools.validate();
                animatePanel.validate();
                animatTools.validate();

                shapePanel.repaint();
                shapeTools.repaint();
                animatePanel.repaint();
                animatTools.repaint();

                showPanel.makeStart();
                repaint();
            }
        });

        pause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PlayingPanel.this.remove(pause);
                PlayingPanel.this.add(play);
                PlayingPanel.this.validate();

                shapePanel.init();
                shapePanel.validate();
                shapePanel.repaint();

                showPanel.makeStop();
                repaint();
            }
        });

    }

}
