 import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    public MainFrame(){

        initframe();

    }

    public void initframe(){

        this.setLocation(new Point(0,0));
        this.setSize(new Dimension(1000,900));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setFocusTraversalKeysEnabled(false);

        ShapeTools shapeTools = new ShapeTools() ;
        AnimatTools animatTools = new AnimatTools() ;
        AnimatePanel animatePanel = new AnimatePanel(animatTools) ;
        ShowPanel showPanel = new ShowPanel(shapeTools,animatePanel,animatTools) ;
        ShapePanel shapePanel = new ShapePanel(showPanel) ;
        PlayingPanel playingPanel = new PlayingPanel(showPanel , animatePanel , animatTools , shapePanel , shapeTools) ;

        this.add(shapePanel,BorderLayout.WEST);
        this.add(showPanel,BorderLayout.CENTER) ;

        JPanel down = new JPanel() ;
        down.setLayout(new GridLayout(0,2));
        this.add(down,BorderLayout.SOUTH);
        down.add(shapeTools);
        down.add(animatTools) ;

        JPanel right = new JPanel() ;
        right.setLayout(new GridLayout(2,0));
        this.add(right,BorderLayout.EAST) ;
        right.add(animatePanel);
        right.add(playingPanel);

    }

}