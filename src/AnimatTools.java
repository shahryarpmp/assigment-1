import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class AnimatTools extends JPanel {

    public AnimatTools() {
        init();
    }

    public void init() {

        this.setBackground(Color.pink);
//        this.setPreferredSize(new Dimension(300, 100));

    }

    public void animateTools(Shap shap, int i) {

        JTextField tool_play = new JTextField();
        tool_play.setText("" + shap.getAnimations()[i].isPlay());
        tool_play.setToolTipText("playing???y/n");
        tool_play.setPreferredSize(new Dimension(50, 20));

        JTextField tool_replay = new JTextField();
        tool_replay.setText("" + shap.getAnimations()[i].isReplay());
        tool_replay.setToolTipText("replaying???y/n");
        tool_replay.setPreferredSize(new Dimension(50, 20));

        JTextField tool_time = new JTextField();
        tool_time.setText("" + shap.getAnimations()[i].getTime());
        tool_time.setToolTipText("time");
        tool_time.setPreferredSize(new Dimension(50, 20));

        tool_time.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_time.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_time.setText("" + shap.getAnimations()[i].getTime());
                } else if (tmp2int != shap.getAnimations()[i].getTime()) {
                    shap.getAnimations()[i].setTime(tmp2int);
                }

            }
        });

        tool_play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_play.getText();

                if (tmp_radius.equals("y")) {
                    shap.getAnimations()[i].setPlay(true);
                    tool_play.setText("" + shap.getAnimations()[i].isPlay());
                } else if (tmp_radius.equals("n")) {
                    shap.getAnimations()[i].setPlay(false);
                    tool_play.setText("" + shap.getAnimations()[i].isPlay());
                } else
                    tool_play.setText("" + shap.getAnimations()[i].isPlay());

            }
        });

        tool_replay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_replay.getText();

                if (tmp_radius.equals("y")) {
                    shap.getAnimations()[i].setReplay(true);
                    tool_replay.setText("" + shap.getAnimations()[i].isReplay());
                } else if (tmp_radius.equals("n")) {
                    shap.getAnimations()[i].setReplay(false);
                    tool_replay.setText("" + shap.getAnimations()[i].isReplay());
                } else
                    tool_replay.setText("" + shap.getAnimations()[i].isReplay());

            }
        });

        this.add(tool_play);
        this.add(tool_replay);
        this.add(tool_time);

    }

    public void magnifiTools(Shap shap) {

        animateTools(shap, 0);

        JTextField tool_magnifi = new JTextField();
        tool_magnifi.setText("" + shap.getAnimations()[0].getMagniSize());
        tool_magnifi.setToolTipText("magnifi size");
        tool_magnifi.setPreferredSize(new Dimension(50, 20));

        tool_magnifi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_magnifi.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_magnifi.setText("" + shap.getAnimations()[0].getMagniSize());
                } else if (tmp2int != shap.getAnimations()[0].getMagniSize()) {
                    shap.getAnimations()[0].setMagniSize(tmp2int);
                }

            }
        });

        this.add(tool_magnifi);

    }

    public void moveTools(Shap shap) {

        animateTools(shap, 1);

        int weight = shap.getAnimations()[1].getBorder()[1];
        int height = shap.getAnimations()[1].getBorder()[0];

        JTextField tool_weight = new JTextField();
        tool_weight.setText("" + weight);
        tool_weight.setToolTipText("weight");
        tool_weight.setPreferredSize(new Dimension(50, 20));

        JTextField tool_height = new JTextField();
        tool_height.setText("" + height);
        tool_height.setToolTipText("height");
        tool_height.setPreferredSize(new Dimension(50, 20));

        tool_height.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_height.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_height.setText("" + shap.getAnimations()[1].getBorder()[0]);
                } else if (tmp2int != shap.getAnimations()[1].getBorder()[0]) {
                    int[] tmpe = new int[2];
                    tmpe[0] = tmp2int;
                    tmpe[1] = shap.getAnimations()[1].getBorder()[1];
                    shap.getAnimations()[1].setBorder(tmpe);

                }

            }
        });

        tool_weight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_weight.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_height.setText("" + shap.getAnimations()[1].getBorder()[0]);
                } else if (tmp2int != shap.getAnimations()[1].getBorder()[1]) {
                    int[] tmpe = new int[2];
                    tmpe[1] = tmp2int;
                    tmpe[0] = shap.getAnimations()[1].getBorder()[0];
                    shap.getAnimations()[1].setBorder(tmpe);

                }

            }
        });

        this.add(tool_weight);
        this.add(tool_height);

    }

    public void picChangerTools(Shap shap) {

        animateTools(shap,2);

        JButton addPic = new JButton();
        addPic.setText("add picture");

        addPic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                PictureChoser pictureChoser = new PictureChoser();

                BufferedImage pic = null;

                if (!pictureChoser.getPath().equals("")) {
                    try {
                        pic = ImageIO.read(new File(pictureChoser.getPath()));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    shap.animpics.add(pic);
                }
            }
        });

        this.add(addPic);

    }

    public void manager(Shap shap) {

        this.clear();

        boolean[] animates = new boolean[3];

        animates[0] = shap.getAnimations()[0].isDefine();
        animates[1] = shap.getAnimations()[1].isDefine();
        animates[2] = shap.getAnimations()[2].isDefine();

        int counter = 0;

        for (int i = 0; i < 3; i++) {
            if (animates[i])
                counter++;
        }

        if (counter > 0) {

            if (animates[0]) {
                magnifiTools(shap);
                JApplet tmp = new JApplet();
                tmp.setPreferredSize(new Dimension(220, 0));
                this.add(tmp);
            }

            if (animates[1]) {
                moveTools(shap);
                JApplet tmp = new JApplet();
                tmp.setPreferredSize(new Dimension(200, 0));
                this.add(tmp);
            }

            if (animates[2]) {
                picChangerTools(shap);
            }

        }

        this.validate();
        this.repaint();

    }

    public void clear() {

        this.removeAll();
        init();

    }

}
