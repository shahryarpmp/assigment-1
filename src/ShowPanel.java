import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class ShowPanel extends JPanel implements ActionListener {

    AnimatePanel animatepanel;
    ShapeTools shapeTools;
    AnimatTools animatTools;

    Shap clickedShape = null;
    Point previousMouseLocation = null;
    Shap selectedShape = null;
    ArrayList<Shap> shaps = new ArrayList<>();

    private Timer timer;
    private int delay = 10;
    boolean start = false;

    public ShowPanel(ShapeTools shapeTools, AnimatePanel animatePanel, AnimatTools animatTools) {
        this.shapeTools = shapeTools;
        this.animatepanel = animatePanel;
        this.animatTools = animatTools;

        setFocusable(true);
        timer = new Timer(delay, this);
        timer.start();

        init();

    }

    public ShowPanel() {
    }

    public void init() {

        this.setBackground(Color.WHITE);
        this.setSize(new Dimension(100, 100));

        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

                panelShower(e);

                Point curLocation = new Point(e.getX(), e.getY());
                Point dif = new Point(curLocation.x - previousMouseLocation.x, curLocation.y - previousMouseLocation.y);
                previousMouseLocation = curLocation;
                if (selectedShape != null) {
                    selectedShape.setLocation(new Point(selectedShape.getLocation().x + dif.x, selectedShape.getLocation().y + dif.y));
                }
                repaint();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                //Point curLocation=new Point(e.getX(),e.getY());
                //Point dif=curLocation.subtract(previousMouseLocation);
            }
        });


        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

                panelShower(e);

            }

            @Override
            public void mousePressed(MouseEvent e) {
                previousMouseLocation = new Point(e.getX(), e.getY());
                Iterator<Shap> iterator = shaps.iterator();
                while (iterator.hasNext()) {
                    Shap cur = iterator.next();
                    if (cur.isIn(previousMouseLocation)) {
                        selectedShape = cur;
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                previousMouseLocation = null;
                selectedShape = null;
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        repaint();

    }

    public void panelShower(MouseEvent e) {
        boolean tmp = false;

        for (int i = 0; i < shaps.size(); i++) {
            if (shaps.get(i).isIn(e.getPoint())) {
                clickedShape = shaps.get(i);
                tmp = true;
                break;
            }
        }

        if (clickedShape.getType() == 1 && tmp) {
            shapeTools.clear();
            shapeTools.circleTools(clickedShape, ShowPanel.this);
        } else if (clickedShape.getType() == 2 && tmp) {
            shapeTools.clear();
            shapeTools.rectTools(clickedShape, ShowPanel.this);
        } else if (clickedShape.getType() == 3 && tmp) {
            shapeTools.clear();
            shapeTools.lineTools(clickedShape, ShowPanel.this);
        } else if (clickedShape.getType() == 4 && tmp) {
            shapeTools.clear();
            shapeTools.picTools(clickedShape, ShowPanel.this);
        }

        if (tmp && clickedShape.getType() == 4) {
            animatepanel.buttuninit(true, clickedShape);
        } else if (tmp) {
            animatepanel.buttuninit(false, clickedShape);
        }

        if (tmp)
            animatTools.manager(clickedShape);

        repaint();

    }

    public void setShaps(ArrayList<Shap> shaps) {
        this.shaps = shaps;
    }

    protected void paintComponent(Graphics G) {

        super.paintComponent(G);
        Iterator<Shap> iterator = shaps.iterator();
        while (iterator.hasNext()) {
            Shap cur = iterator.next();
            cur.paint(G);
        }

    }

    public void makeStop() {
        start = false;
        timer.stop();
    }

    public void makeStart() {

        Iterator<Shap> iterator = shaps.iterator();
        while (iterator.hasNext()) {
            Shap cur = iterator.next();
            if (cur.getAnimations()[0].isPlay()) {
                cur.magniPerSec();
            }
            if (cur.getAnimations()[1].isPlay())
                cur.movePerSec();
        }

        start = true;
        timer.start();

    }

    public void animating() {

        Iterator<Shap> iterator = shaps.iterator();
        while (iterator.hasNext()) {
            Shap cur = iterator.next();

            if (cur.getAnimations()[0].isPlay())
                cur.step(1);

            if (cur.getAnimations()[1].isPlay())
                cur.step(2);

            if (cur.getAnimations()[2].isPlay()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                cur.step(3);
            }
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (start) {
            animating();
            repaint();
        } else {
            timer.stop();
        }
    }

}