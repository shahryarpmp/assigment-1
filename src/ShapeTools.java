import sun.security.provider.SHA;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

public class ShapeTools extends JPanel {

    public ShapeTools() {
        init();
    }

    public void init() {

        this.setBackground(Color.gray);
        this.setPreferredSize(new Dimension(100, 200));

    }

    public void circleTools(Shap shap, ShowPanel showPanel) {

        int radius = shap.getWeight();
        JTextField tool = new JTextField();
        tool.setText("" + radius);
        tool.setToolTipText("radius");
        tool.setPreferredSize(new Dimension(50, 20));

        tool.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool.setText("" + radius);
                } else if (tmp2int != radius) {
                    shap.setWeight(tmp2int);
                    shap.setHeight(tmp2int);
                    showPanel.repaint();
                }

            }
        });

        this.add(tool);
        this.validate();
        this.repaint();

    }

    public void rectTools(Shap shap, ShowPanel showPanel) {

        int weight = shap.getWeight();
        int height = shap.getHeight();

        JTextField tool_weight = new JTextField();
        tool_weight.setText("" + weight);
        tool_weight.setToolTipText("weight");
        tool_weight.setPreferredSize(new Dimension(50, 20));

        JTextField tool_height = new JTextField();
        tool_height.setText("" + height);
        tool_height.setToolTipText("height");
        tool_height.setPreferredSize(new Dimension(50, 20));

        tool_height.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_height.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_height.setText("" + height);
                } else if (tmp2int != height) {
                    shap.setHeight(tmp2int);
                    showPanel.repaint();
                }

            }
        });

        tool_weight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_weight.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_weight.setText("" + weight);
                } else if (tmp2int != weight) {
                    shap.setWeight(tmp2int);
                    showPanel.repaint();
                }

            }
        });

        this.add(tool_weight);
        this.add(tool_height);
        this.validate();
        this.repaint();

    }

    public void lineTools(Shap shap, ShowPanel showPanel) {

        int weight = shap.getWeight();
        int height = shap.getHeight();

        JTextField tool_weight = new JTextField();
        tool_weight.setText("" + weight);
        tool_weight.setToolTipText("weight");
        tool_weight.setPreferredSize(new Dimension(50, 20));

        JTextField tool_height = new JTextField();
        tool_height.setText("" + height);
        tool_height.setToolTipText("height");
        tool_height.setPreferredSize(new Dimension(50, 20));

        tool_height.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_height.getText();
                int tmp2int = 0;
                boolean tmp = true ;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                    tool_height.setText("" + height);
                    tmp = false ;
                }

                if (tmp2int != height && tmp ) {
                    if (tmp2int >= 0)
                        shap.setHeight(tmp2int);
                    else {
                        shap.setHeight(tmp2int);
                        shap.setY( shap.getY() - tmp2int);
                    }
                    showPanel.repaint();
                }

            }
        });

        tool_weight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_weight.getText();
                int tmp2int = 0;
                boolean tmp = true ;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                    tool_weight.setText("" + weight);
                    tmp = false ;
                }

                if (tmp2int != weight && tmp) {
                    if (tmp2int >= 0)
                        shap.setWeight(tmp2int);
                    else {
                        shap.setWeight(tmp2int);
                        shap.setX(shap.getX() - tmp2int);
                    }
                    showPanel.repaint();
                }

            }
        });

        this.add(tool_weight);
        this.add(tool_height);
        this.validate();
        this.repaint();

    }

    public void picTools(Shap shap, ShowPanel showPanel) {

        int weight = shap.getWeight();
        int height = shap.getHeight();

        JTextField tool_weight = new JTextField();
        tool_weight.setText("" + weight);
        tool_weight.setToolTipText("weight");
        tool_weight.setPreferredSize(new Dimension(50, 20));

        JTextField tool_height = new JTextField();
        tool_height.setText("" + height);
        tool_height.setToolTipText("height");
        tool_height.setPreferredSize(new Dimension(50, 20));

        tool_height.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_height.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_height.setText("" + height);
                } else if (tmp2int != height) {
                    shap.scale(shap.getPic() , shap.getWeight() , tmp2int);
                    showPanel.repaint();
                }

            }
        });

        tool_weight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String tmp_radius = tool_weight.getText();
                int tmp2int = -1;

                try {
                    tmp2int = Integer.parseInt(tmp_radius);
                } catch (IllegalArgumentException e1) {
                }

                if (tmp2int <= 0) {
                    tool_weight.setText("" + weight);
                } else if (tmp2int != weight) {
                    shap.scale(shap.getPic() , tmp2int , shap.getHeight()) ;
                    showPanel.repaint();
                }

            }
        });

        this.add(tool_weight);
        this.add(tool_height);
        this.validate();
        this.repaint();


    }

    public void clear() {

        this.removeAll();
        init();

    }
}
