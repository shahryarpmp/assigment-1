import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AnimatePanel extends JPanel {

    private AnimatTools animatTools = new AnimatTools() ;

    public AnimatePanel(){}

    public AnimatePanel(AnimatTools animatTools){
        this.animatTools = animatTools ;
        init();
    }

    private void init() {
        this.setBackground(Color.BLUE);
//        this.setPreferredSize(new Dimension(100,300));
    }

    public void buttuninit(boolean tmp, Shap shap) {

        this.clear();

        JButton magnifier = new JButton();
        JButton mover = new JButton();
        JButton picChanger = new JButton();

        Path currentRelativePath = Paths.get("");
        String path = currentRelativePath.toAbsolutePath().toString() + "/src/pics/";

        //
        BufferedImage magnifierImg = null;
        try {
            magnifierImg = ImageIO.read(new File(path + "magnifier.png" ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Graphics2D g2 = magnifierImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.drawImage(magnifierImg, 0, 0, null);
        g2.dispose();
        magnifier.setIcon(new ImageIcon(magnifierImg));
        this.add(magnifier);

        //
        BufferedImage arrowImg = null;
        try {
            arrowImg = ImageIO.read(new File(path + "arrow.png" ));
        } catch (IOException e) {
            e.printStackTrace();
        }
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.drawImage(arrowImg, 0, 0, null);
        g2.dispose();
        mover.setIcon(new ImageIcon(arrowImg));
        this.add(mover);

        mover.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                shap.getAnimations()[1].setDefine(true);
                animatTools.manager(shap);

            }
        });

        magnifier.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                shap.getAnimations()[0].setDefine(true);
                animatTools.manager(shap);

            }
        });

        if ( tmp ) {

            BufferedImage changerImg = null;
            try {
                changerImg = ImageIO.read(new File(path + "changer.png"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.drawImage(changerImg, 0, 0, null);
            g2.dispose();
            picChanger.setIcon(new ImageIcon(changerImg));
            this.add(picChanger);

            picChanger.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    shap.getAnimations()[2].setDefine(true);
                    animatTools.manager(shap);

                }
            });

        }

        this.validate();
        this.repaint();

    }

    public void clear(){
        this.removeAll();
        init();
    }

}
