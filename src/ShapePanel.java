import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ShapePanel extends JPanel {

    private ShowPanel showpanel;

    ArrayList<Shap> shapes = new ArrayList<>();

    private int height = 20;
    private int weight = 20;
    private int x = 10;
    private int y = 10;

    public ShapePanel(ShowPanel shwpnl) {
        this.showpanel = shwpnl;
        init();
    }

    public ShapePanel() {
        init();
    }

    public void init() {

        this.setBackground(Color.red);
        this.setPreferredSize(new Dimension(100, 100));

        JButton rectangle = new JButton();
        JButton circle = new JButton();
        JButton line = new JButton();
        JButton arc = new JButton();

        int IMG_WIDTH = 30; //this.getWidth()+this.getWidth()/2 ;
        int IMG_HEIGHT = 30; //this.getHeight()+this.getHeight()/2 ;
        BufferedImage circleImg = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = circleImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.BLACK);
        int x = 4;
        int y = x;
        int width = 25;//IMG_WIDTH - 2 * x;
        int height = 25;//IMG_HEIGHT - 2 * y;
        g2.fillOval(x, y, width, height);
        g2.dispose();
        circle.setIcon(new ImageIcon(circleImg));

        //
        BufferedImage squareImg = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = squareImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.black);
        g2.fillRect(x, y, width, height);
        g2.dispose();
        rectangle.setIcon(new ImageIcon(squareImg));

        //
        BufferedImage lineImg = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = lineImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.black);
        g2.drawLine(x, y, x + width, y + height);
        g2.dispose();
        line.setIcon(new ImageIcon(lineImg));

        //
        BufferedImage arcImg = new BufferedImage(IMG_WIDTH, IMG_WIDTH, BufferedImage.TYPE_INT_ARGB);
        g2 = arcImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(Color.black);
        g2.fillArc(x, y, width, height, 45, 260);
        g2.dispose();
        arc.setIcon(new ImageIcon(arcImg));

        this.add(arc);
        this.add(line);
        this.add(rectangle);
        this.add(circle);

        circle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Shap shape = new Shap(x, y, height, weight, 1);
                shapes.add(shape);
                showpanel.shaps.add(shape);
                showpanel.repaint();

            }
        });

        rectangle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Shap shape = new Shap(x, y, height, weight, 2);
                shapes.add(shape);
                showpanel.shaps.add(shape);
                showpanel.repaint();

            }
        });

        line.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Shap shape = new Shap(x, y, height, weight, 3);
                shapes.add(shape);
                showpanel.shaps.add(shape);
                showpanel.repaint();
            }
        });

        arc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                PictureChoser picchoos = new PictureChoser();
                if (!picchoos.getPath().equals("")) {
                    Shap shape = new Shap(x, y, height, weight, 4);
                    shape.setPath(picchoos.getPath());

                    try {
                        shape.setPic(ImageIO.read(new File(shape.getPath())));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                    shapes.add(shape);
                    shape.animpics.add(shape.getPic());
                    showpanel.shaps.add(shape);
                    showpanel.repaint();
                }
            }
        });

    }

    public ArrayList<Shap> getShapes() {
        return shapes;
    }

    public void clear() {
        this.removeAll();
        this.setBackground(Color.red);
        this.setPreferredSize(new Dimension(100, 100));

    }
}