import sun.security.provider.SHA;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Shap {

    private int x = 0;
    private int y = 0;
    private int weight = 20;
    private int height = 20;
    private int type; // 1circle 2rectangle 3line 4picture
    private double id;
    private Animation[] animations = new Animation[3];

    int[] magnifiPerSecSaver = new int[2]; // for magnifing
    int[] movePerSecSaver = new int[2]; // for moving
    int[] magnifirst = new int[2]; // for magnifing
    int[] movefirst = new int[2]; // for moving
    int[] magnifilast = new int[2]; // for magnifing
    int[] movelast = new int[2]; // for moving
    boolean magni = false;
    boolean move = false;

    BufferedImage drawingpic = null;
    private BufferedImage pic = null; // just fpr picturs
    private String path = ""; // just for picturs
    ArrayList<BufferedImage> animpics = new ArrayList<>(); // for picture changer of pictures
    int picchager = 0;
    boolean picchngr = false;

    public Shap(int type) {
        this.type = type;
        id = Math.random();
    }

    public Shap(int x, int y, int weight, int height, int type) {
        id = Math.random();
        this.x = x;
        this.y = y;
        this.weight = weight;
        this.height = height;
        this.type = type;
        animationinit();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        if (this.getType() == 4)
            this.path = path;
    }

    public BufferedImage getPic() {
        return pic;
    }

    public void setPic(BufferedImage pic) {

        this.pic = pic;
        this.setHeight(pic.getHeight());
        this.setWeight(pic.getWidth());
        this.drawingpic = pic;

    }

    public boolean isIn(Point a) {

        if (Math.abs(a.x - x) < Math.abs(weight) && Math.abs(a.y - y) < Math.abs(height))
            return true;
        return false;

    }

    public Point getLocation() {
        return new Point(x, y);
    }

    public void setLocation(Point a) {
        x = a.x;
        y = a.y;
    }

    public double getId() {
        return id;
    }

    public boolean equals(Shap shap) {

        if (shap.getId() == this.getId())
            return true;
        return false;

    }

    public void scale(BufferedImage src, int w, int h) {

        BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

        int i, j;
        int srcWidth = src.getWidth();
        int srcHeigh = src.getHeight();
        int[] ys = new int[h];
        for (i = 0; i < h; i++)
            ys[i] = i * srcHeigh / h;
        for (j = 0; j < w; j++) {
            int newx = j * srcWidth / w;
            for (i = 0; i < h; i++) {
                int col = src.getRGB(newx, ys[i]);
                img.setRGB(j, i, col);
            }
        }

        this.setPic(img);

    }

    public void animationinit() {

        animations[0] = new Animation();
        animations[1] = new Animation();
        animations[2] = new Animation();

    }

    public void paint(Graphics g) {

        if (type == 1) {
            g.setColor(Color.black);
            g.fillOval(this.getX(), this.getY(), this.getWeight(), this.getHeight());
        }

        if (type == 2)
            g.fillRect(this.getX(), this.getY(), this.getWeight(), this.getHeight());

        if (type == 3)
            g.drawLine(this.getX(), this.getY(), this.getX() + this.getWeight(), this.getY() + this.getHeight());

        if (type == 4)
            g.drawImage(drawingpic, this.getX(), this.getY(), null);

    }

    public Animation[] getAnimations() {
        return animations;
    }

    public void magniPerSec() {

        magnifiPerSecSaver[0] = (animations[0].getMagniSize() * height - height) / animations[0].getTime();
        magnifiPerSecSaver[1] = (animations[0].getMagniSize() * weight - weight) / animations[0].getTime();

        magnifilast[0] = (animations[0].getMagniSize() * height);
        magnifilast[1] = (animations[0].getMagniSize() * weight);

        magnifirst[0] = height;
        magnifirst[1] = weight;

    }

    public void movePerSec() {

        movePerSecSaver[0] = (animations[1].getBorder()[0]) / animations[1].getTime();
        movePerSecSaver[1] = (animations[1].getBorder()[1]) / animations[1].getTime();

        movelast[0] = (animations[1].getBorder()[0] + x);
        movelast[1] = (animations[0].getBorder()[1] + y);

        movefirst[0] = x;
        movefirst[1] = y;

    }

    public void step(int i) {

        if (i == 1 && animations[0].isPlay()) {

            if (height < magnifilast[0] && !magni) {
                height += magnifiPerSecSaver[0];
                weight += magnifiPerSecSaver[1];

                if (type == 4)
                    scale(getPic(), weight, height);
            } else if (animations[0].isReplay()) {

                if (height != magnifirst[0]) {
                    magni = true;
                    height -= magnifiPerSecSaver[0];
                    weight -= magnifiPerSecSaver[1];

                    if (type == 4)
                        scale(getPic(), weight, height);
                } else
                    magni = false;
            }
        }

        if (i == 2 && animations[1].isPlay()) {

            if (x < movelast[0] && !move) {
                x += movePerSecSaver[0];
                y += movePerSecSaver[1];
            } else if (animations[1].isReplay()) {
                if (x != movefirst[0]) {
                    move = true;
                    x -= movePerSecSaver[0];
                    y -= movePerSecSaver[1];
                } else
                    move = false;
            }
        }

        if (i == 3 && animations[2].isPlay()) {

            if (picchager < animpics.size() - 1 && !picchngr) {
                drawingpic = animpics.get(++picchager);
            } else if (animations[2].isReplay()) {
                if (picchager != 0) {
                    picchngr = true;
                    drawingpic = animpics.get(--picchager);
                } else
                    picchngr = false;
            }

        }

    }

}