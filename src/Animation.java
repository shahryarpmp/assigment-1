import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Animation {

    private boolean define = false ;
    private boolean play = false;
    private int time;
    private boolean replay;
    private int type; // 1- magnifier 2- mover 3- pic-changer
    private int magniSize; // magnifier
    private int[] border = new int[2]; //height width //mover
    private ArrayList<BufferedImage> images = new ArrayList<>(); //pic-changer

    public boolean isPlay() {
        return play;
    }

    public void setPlay(boolean play) {
        this.play = play;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public boolean isReplay() {
        return replay;
    }

    public void setReplay(boolean replay) {
        this.replay = replay;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getMagniSize() {
        return magniSize;
    }

    public void setMagniSize(int magniSize) {
        this.magniSize = magniSize;
    }

    public int[] getBorder() {
        return border;
    }

    public void setBorder(int[] border) {
        this.border[0] = border[0];
        this.border[1] = border[1];
    }

    public ArrayList<BufferedImage> getImages() {
        return images;
    }

    public void setImages(ArrayList<BufferedImage> images) {
        this.images = images;
    }

    public boolean isDefine() {
        return define;
    }

    public void setDefine(boolean define) {
        this.define = define;
    }
}
